import java.util.ArrayList;
import java.util.List;

import br.com.davesmartins.grafos.grafos.lib.base.grafo.ETipoGrafo;
import br.com.davesmartins.grafos.grafos.lib.base.grafo.Vertice;
import br.com.davesmartins.grafos.grafos.lib.base.impl.GraphBaseListaAdjacencia;

public class Graph extends GraphBaseListaAdjacencia{
	
	private ArrayList<ArrayList<Vertice>> listaAdj = new ArrayList<ArrayList<Vertice>>();
	Vertice vertice = new Vertice();
	
	
	@Override
	public Object[] getListaAdjacencia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getListaAdjacenciaToString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changeTipoGrafo(ETipoGrafo arg0) {
		// TODO Auto-generated method stub
		
	}
        
        @Override
        public void clickAddAresta(Vertice origem, Vertice destino) {
                for(ArrayList<Vertice> vertices : listaAdj)
                {
                    if(vertices.get(0).getNome() == origem.getNome())
                    {
                        vertices.add(destino);
                    }
                }
        }
	
	public void clickAddAresta(String origem, String destino) {
		Vertice o = new Vertice(origem);
                Vertice d = new Vertice(destino);
                clickAddAresta(o, d);
	}
        

	@Override
	public void clickAddVertice(String nome) {
		Vertice e = new Vertice(nome);
                clickAddVertice(e);
	}
        
        public void clickAddVertice(Vertice e) {
                ArrayList<Vertice> vertice = new  ArrayList<Vertice>();
		vertice.add(e);
		listaAdj.add(vertice);
        }

	@Override
	public int getGrau(Vertice e) {
		int grau = 0;
                grau = getGrauEmissao(e);
                grau += getGrauRecpcao(e);
		return grau;
	}

	@Override
	public int getGrauEmissao(Vertice e) {
                 int grau = -1;
                 
		 for(ArrayList<Vertice> vertices : listaAdj)
                {
                    if(vertices.get(0).getNome() == e.getNome())
                    {
                        for(Vertice tamanho : vertices)
                        {
                           grau++;
                        }
                    }
                }
		return grau;
	}

	@Override
	public int getGrauRecpcao(Vertice e) {
		int grau = -1;
                for(ArrayList<Vertice> vertices : listaAdj)
                {
                    for(int i = 0; i < vertices.size(); i++)
                    {
                        if(vertices.get(i).getNome() == e.getNome())
                        {
                            grau++;
                        }
                    }
                }
		return grau;
	}

	@Override
	public String getGrupo() {
		// our group's name
		return "Drag�osDeKomodo.rar";
	}

	
	@Override
	public String[] getMembros() {
		// our group members
		return new String[] {"Jafar Untar", "Raphael Tavares"};
	}

	@Override
	public int getNumeroArestas() {
		int arestas = 0;
		for(ArrayList<Vertice> vertices : listaAdj) 
		{
			arestas += vertices.size();
		}
		arestas -= listaAdj.size();

		return arestas;
	}

	@Override
	public int getOrdem() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Vertice> getVertices() {
		// TODO Auto-generated method stub
		return null;
	}

}
