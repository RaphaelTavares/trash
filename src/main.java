
import br.com.davesmartins.grafos.grafos.lib.base.grafo.Vertice;


public class main {

	public static void main(String[] args) {
		Graph myGraph = new Graph();
		
                Vertice verticeA = new Vertice("A");
                myGraph.clickAddVertice(verticeA);
                
                Vertice verticeB = new Vertice("B");
                myGraph.clickAddVertice(verticeB);
                
                Vertice verticeC = new Vertice("C");
                myGraph.clickAddVertice(verticeC);
                
                myGraph.clickAddAresta(new Vertice("A"), new Vertice("B"));
                myGraph.clickAddAresta("A", "C");
                myGraph.clickAddAresta("C", "A");
                myGraph.clickAddAresta("B", "C");
                
                String[] membros = myGraph.getMembros();
                
                System.out.println("Nome do grupo: " + myGraph.getGrupo() + "\nParticipantes: " + membros[0] + " e " + membros[1]);
                int grauEmissaoA = myGraph.getGrauEmissao(verticeA);
                System.out.println("Grau de emiss�o do v�rtice A: " + grauEmissaoA);
                
                int grauRecepcaoB = myGraph.getGrauRecpcao(verticeB);
                System.out.println("Grau de recep��o do v�rtice B: " + grauRecepcaoB);
                
                int grauGeralA = myGraph.getGrau(verticeA);
                System.out.println("Grau geral do v�rtice A: " + grauGeralA);
                
                int arestasGrafo = myGraph.getNumeroArestas();
                System.out.println("N�mero de arestas do grafo: " + arestasGrafo);
	}
}

